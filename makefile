all: silent_compile run

compile: 
	@docker build -t my-java-app .

silent_compile:
	@docker build -t my-java-app . > /dev/null

run:
	@docker run -it --rm --name my-running-app my-java-app
